import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Navbar from './Components/Navbar';
import Products from './Components/Products';
import CardDetail from './Components/CardDetail';
import Checkout from './Components/Checkout';
import { CartProvider } from './CardContext';

const App = () => {
  return (
    <div>
      <CartProvider>
        <Router>
          <Navbar />
          <Routes>
            <Route path='/' element={<Products />} />
            <Route path='chekout' element={<Checkout />} />
            <Route path='products/:id' element={<CardDetail />} />
          </Routes>
        </Router>
      </CartProvider>
    </div>
  );
};

export default App;
