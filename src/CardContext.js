import { createContext, useState } from 'react'

const CartContext = createContext()
export function CartProvider({ children }) {
  const [items, setItems] = useState([])

  const addToCard = (item, id) => {
    setItems((prev) => [...prev, { item, id }])
  }

  return (
    <div>
      <CartContext.Provider value={{ items, addToCard }}>
        {children}
      </CartContext.Provider>
    </div>
  )
}

export default CartContext