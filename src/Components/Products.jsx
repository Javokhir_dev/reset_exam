import React, { useState, useEffect } from 'react';
import Card from './Card';
import './Style.css'

const Products = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true)
  useEffect(() => {
    fetch('https://dummyjson.com/products')
      .then((res) => res.json())
      .then((prod) => {
        setData(prod.products)
        setLoading(false)
      })
      .catch(error => {
        console.log("Error fetch data: " + error);
        setLoading(false)
      })
  }, []);

  console.log(data);

  return (
    <div className="container">
      <div className='row '>
        {loading ? <h2>Loading...</h2> : data.map((item, id) => (
          <Card item={item} id={id} key={id} />
        ))}
      </div>
    </div>
  )
};

export default Products;
