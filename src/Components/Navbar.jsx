import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { HiOutlineShoppingCart } from "react-icons/hi"
import CartContext from '../CardContext'
const Navbar = () => {
    const { items } = useContext(CartContext)
    return (

        <div>
            <div>
                <nav className="navbar bg-body-tertiary py-3 px-4">
                    <div className="container-fluid">
                        <Link to={"/"} className="navbar-brand">
                            <h5>Home</h5>
                        </Link>
                        <Link to={"chekout"}>
                            <button className="d-flex btn btn-primary gap-2 align-items-center">
                                <HiOutlineShoppingCart />
                                <span>{items.length}</span>

                            </button>
                        </Link>

                    </div>
                </nav>
            </div>
        </div>
    )
}

export default Navbar