import React from 'react';
import { Link } from 'react-router-dom';

const Card = ({ item, id }) => {
  return (
    <Link to={`/products/${id + 1}`} className='col-md-3 p-3 back__btn' key={id}>
      <div className="card">
        <img src={item.thumbnail} className="card-img-top" alt={item.title} style={{ height: "250px", objectFit: "cover" }} />
        <div className="card-body">
          <h4 className="card-title">{item.title.slice(0, 20)}</h4>
          <h5 className='py-2'>{item.price} $</h5>
          <p className="card-text">{item.description.slice(0, 50)}</p>
          <Link to={`/products/${id + 1}`} className="btn btn-primary">Product Info</Link>
        </div>
      </div>
    </Link>
  );
}

export default Card;
