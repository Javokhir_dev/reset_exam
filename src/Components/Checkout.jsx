import React, { useContext } from 'react'
import CartContext from '../CardContext'
import './Style.css'

const Checkout = () => {
  const { items } = useContext(CartContext)
  const total = items.reduce((acc, item) => acc + item.item.post.price, 0)
  console.log(items);
  return (
    <div>
      {items.map((item) => (
        <div key={item.item.post.id} className='container'>
          <div className='box-shop'>
            <img src={item.item.post.thumbnail} alt="" />
            <p className='title'>{item.item.post.title}</p>
            <p className='price'>${item.item.post.price}</p>
          </div>
        </div>
      ))}
      <h5 className='container my-4 text-end'>Total price: ${total}</h5>
    </div>
  )
}

export default Checkout